TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -O3

SOURCES += structures.cpp \
    solutionevaluator.cpp \
    main.cpp

HEADERS += \
    loaders.hpp \
    structures.hpp \
    utils.hpp \
    solutionevaluator.h

DISTFILES += \
    Scheduler.pro.user \
    input.txt
