#ifndef LOADERS_HPP
#define LOADERS_HPP

#include "structures.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

int classroomCount=0;

Group getGroup(ifstream& ifs)
{
    Group group;
    ifs >> group.id >> group.name;
    cout << "Grupa[" << group.id << "]: " << group.name <<endl;
    return group;
}

Teacher getTeacher(ifstream& ifs)
{

    Teacher teacher;

    ifs >> teacher.id ;
    ifs.ignore();
    string line;
    getline( ifs, line );
    //boost::algorithm::trim_right(line);
    teacher.name = line;
    getline( ifs, line );
    //boost::algorithm::trim_right(line);
    cout << "Prowadzacy[" << teacher.id << "]: " << teacher.name << ": ";
    vector<string> days;

    istringstream iss(line);
    while( true )
    {
        int buff;
        iss >> buff;
        if( iss.fail() )
            break;
        teacher.availableWeekDays.push_back(static_cast<WeekDay>(buff));

        //availability to dostepnosc w slotach, wiec trzeba na nie przeliczyc dni
        fill( teacher.availability.begin()+buff*SLOTS_PER_DAY,
              teacher.availability.begin()+(buff+1)*SLOTS_PER_DAY,
              true );
        cout << WEEK_DAY_NAMES[static_cast<WeekDay>(buff)] << " ";
    }
    cout << endl;
    return teacher;
}

Classroom getClassroom(ifstream& ifs)
{
    Classroom cr;
    ifs >> cr.id ;
    ifs.ignore();
    //getline( ifs, cr.place );
    ifs >> cr.address;
    ifs.ignore();
    getline( ifs, cr.place );
    //boost::algorithm::trim_right(cr.place);
    cout << "Sala[" << cr.id << "]: " << cr.address << " - " << cr.place << endl;
    if( classroomCount != cr.id )
        cout<<"ERROR: classroom id in wrong order"<<endl;
    classroomCount++;
    return cr;
}

Subject getSubject(ifstream& ifs)
{
    Subject subject;
    ifs >> subject.id;
    ifs.ignore();

    getline( ifs, subject.name );

    ifs >> subject.lessonsPerWeek >> subject.idTeacher >> subject.idGroup;
    cout << "Przedmiot[" << subject.id << "] "<< subject.name << " lpw:" //lessonsPerWeek
         << subject.lessonsPerWeek << ", teacher["
         << subject.idTeacher << "], group[" <<  subject.idGroup << "], ";
    ifs.ignore();

    string classroomAvailibility;
    getline(ifs, classroomAvailibility);

    cout << "classroom requirements: ";

    if("*" == classroomAvailibility)
    {
        //zalozenie, ze id sal ida po kolei
        subject.classRoomsAvalibility.resize( classroomCount );
        std::iota(subject.classRoomsAvalibility.begin(),
                  subject.classRoomsAvalibility.end(),
                  0);

        cout << "any";
    }
    else
    {
        cout << "only ";
        istringstream iss(classroomAvailibility);
        while(true)
        {
            int buff;
            iss >> buff;
            if( iss.fail() )
                break;
            subject.classRoomsAvalibility.push_back(buff);
            cout << buff << ", ";
        }
    }

    for (int i=0;i<subject.lessonsPerWeek;i++) {
        Lesson lesson (subject.id, subject.idTeacher);
        subject.lessons.push_back(lesson);
    }

    cout << endl;
    return subject;
}

#endif // LOADERS_HPP
