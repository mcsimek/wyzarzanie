#ifndef SOLUTIONEVALUATOR_H
#define SOLUTIONEVALUATOR_H

#include "structures.hpp"

class SolutionEvaluator
{
public:
    SolutionEvaluator(Days const& days, Classrooms const& classrooms)
        :  g_days(days), g_classrooms(classrooms) {}

    int operator()(Solution const& solution) const;

private:

    int get_address_and_place_points(Solution const& solution) const;
    int get_window_and_subjects_points(Solution const& solution) const;

    const Days& g_days;
    const Classrooms& g_classrooms;
};

#endif // SOLUTIONEVALUATOR_H
