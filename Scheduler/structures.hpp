#ifndef STRUCTURES_HPP
#define STRUCTURES_HPP

#include <vector>
#include <string>
#include <algorithm>
#include <stdexcept>
using namespace std;

/////////////////////////
///// CONSTANTS
/////////////////////////
const int WORK_DAYS = 5;
const int SLOTS_PER_DAY = 5;

enum WeekDay
{
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday/*,
    Saturday,
    Sunday*/
};

constexpr const char * WEEK_DAY_NAMES[] = { "Monday" , "Tuesday",
                                          "Wednesday", "Thursday",
                                          "Friday",    "Saturday", "Sunday"};

constexpr const char * SLOT_NAMES[] =
{ "8:00-9:30", "9:30-11:00", "11:15-12:45","12:45-14:15","14:30-16:00" };


template<typename RandomAccessContainer, typename Elem>
bool contains(RandomAccessContainer container, Elem elem)
{
    return (find(begin(container), end(container), elem) != end(container));
}

/////////////////////////
///// CLASSES AND STRUCTURES
/////////////////////////
struct Group
{
    int id{};
    string name{};
    Group()
    {
        availability.resize(WORK_DAYS*SLOTS_PER_DAY);
        fill(availability.begin(), availability.end(), true);
    }

    bool checkAvailability(int slotId) const
    {
        if(slotId >= availability.size())
            throw logic_error("errrorrr!");

        return availability.at(slotId);
    }

    void setAvailability(int slotId, bool availability_)
    {
        this->availability[slotId] = availability_;
    }

    vector<bool> availability{};
};

using Groups = vector<Group>;


struct Teacher
{
    Teacher()
    {
        availability.resize(WORK_DAYS*SLOTS_PER_DAY);
        fill(availability.begin(), availability.end(), false);
    }

    bool checkAvailability(int slotId) const
    {
        return availability[slotId];
    }

    void setAvailability(int slotId, bool availability_)
    {
        this->availability[slotId] = availability_;
    }

    int id;
    string name; //string surname?
    vector<WeekDay> availableWeekDays;
    vector<bool> availability;
};
using Teachers = vector<Teacher>;


struct Classroom
{
    int id{};
    string place{};
    string address{};

    Classroom()
    {
        availability.resize(WORK_DAYS*SLOTS_PER_DAY);
        fill(availability.begin(), availability.end(), true);
    }

    bool checkAvailability(int slotId) const
    {
        return availability.at(slotId);
    }

    void setAvailability(int slotId, bool availability_)
    {
        this->availability[slotId] = availability_;
    }

    vector<bool> availability;
};
using Classrooms = vector<Classroom>;


struct Lesson
{
    Lesson(int subjectId_, int teacherId_) :
        id(idCounter++), subjectId(subjectId_), id_teacher(teacherId_) {}

    int id{};
    int subjectId{};
    int id_group{}, id_teacher{}, id_classroom{}, id_slot{};
private:
    static int idCounter;
};

using Lessons = vector<Lesson>;


struct Subject
{
    int id{};
    string name{};
    int lessonsPerWeek{};
    int idTeacher{};
    int idGroup{};
    vector<int> classRoomsAvalibility{};
    Lessons lessons;

    bool checkClassroomApplicable(int classroomId)
    {
        return contains(classRoomsAvalibility, classroomId);
    }
};
using Subjects = vector<Subject>;


struct Slot
{
    Slot(string text_) :
        id(idCounter++), text(text_) {}

    int id{};
    string text{};
    bool free = true;
    vector<Lesson> lessons{};
private:
    static int idCounter;
};
using Slots = vector<Slot>;

struct Day
{
    Day(int id_) : id(id_), name( WEEK_DAY_NAMES[id_])
    {
        for(int i=0; i<SLOTS_PER_DAY;i++)
            slots.push_back(Slot(SLOT_NAMES[i]));
    }

    int id{};
    string name{};
    Slots slots{};
};
using Days = vector<Day>;


///////////////
/// UTILS
///



struct Solution
{
    Groups groups{};
    Classrooms classrooms{};
    Subjects subjects{};
    Lessons lessons{};

    mutable int energy = 0;
};

#endif // STRUCTURES_HPP
