#include "solutionevaluator.h"
#include <algorithm>
#include <functional>
#include <map>

int SolutionEvaluator::operator()(const Solution& solution) const
{
    solution.energy = 0;

    solution.energy += get_window_and_subjects_points(solution);
    solution.energy += get_address_and_place_points(solution);
//    solution.energy += get_subject_points(solution);

    return solution.energy;
}

int SolutionEvaluator::get_address_and_place_points(Solution const& solution) const
{
    int points_for_same_classroom = 10;
    int points_for_same_address = 5;

    int points_for_subjects[] = { 0, 5, 0, -20, -50, -100, -150, -200};


    int score = 0;
    for (auto const& group : solution.groups)
    {
        // cout << "next_group:"<<endl;
        for(auto const& day: g_days)
        {
            // cout << "next day:" << endl;
            map<int, int> subjects_counters;
            string address{}, place{};
            for(auto const& slot : day.slots)
            {
                if(!group.checkAvailability(slot.id))
                {
                    for(auto const& lesson : solution.lessons)
                    {
                        if (lesson.id_group==group.id && lesson.id_slot == slot.id)
                        {
                            // cout<<  "sala: " << g_classrooms[lesson.id_classroom].address << " " << g_classrooms[lesson.id_classroom].place << endl;
                            if(!address.empty() and !place.empty())
                                if(address == g_classrooms[lesson.id_classroom].address)
                                {
                                    score += points_for_same_address;
                                    if(place == g_classrooms[lesson.id_classroom].place)
                                        score += points_for_same_classroom;
                                }
                            address = g_classrooms[lesson.id_classroom].address;
                            place = g_classrooms[lesson.id_classroom].place;


                        subjects_counters[lesson.subjectId]++;


                        }
                    }

                }


                for(auto& entry : subjects_counters)
                    score += points_for_subjects[entry.second];

            }
        }

    }

    return score;
}

int SolutionEvaluator::get_window_and_subjects_points(Solution const& solution) const
{
    const int points_for_one_slot_window_in_day = -8;
    const int points_for_free_day = 10;
    const int points_for_full_day = -3;

    int score = 0;
    for (auto const& group: solution.groups)
    {
        // cout << "NAME = " << group.name << endl;
        auto startDay = group.availability.begin();
        auto endDay = group.availability.begin() + SLOTS_PER_DAY;

        while (startDay != group.availability.end())
        {

            //per day:
            int busy = count_if(startDay, endDay, [](bool a){return not a;});
            // cout << "busy  = " << busy ;
            if (busy == SLOTS_PER_DAY)
                score += points_for_full_day;
            else if(busy == 0)
                score += points_for_free_day;
            else
            {
                //TODO znalezc ostatnia zajeta lekcje rano i pierwsza wieczorem!
                //teraz jest ze szuka pierwszej zejetej rano i ostatniej wieczorem :P
                // cout << ", not fulll, not empty: ";
                auto first_busy = find(startDay, endDay, false) - startDay;
                // auto first_last_busy = find(first_busy_it, endDay, true) - startDay;
                using crev_bool_it = vector<bool>::const_reverse_iterator;
                auto last_busy = SLOTS_PER_DAY - (find(crev_bool_it(endDay), crev_bool_it(startDay), false) - crev_bool_it(endDay));
                // auto last_first_busy = (find(last_busy, vector<bool>::const_reverse_iterator(startDay), true) - last_busy);//vector<bool>::const_reverse_iterator(startDay));

                // cout << "first_busy = " <<first_busy << ", last_first_busy = " << last_busy ;
                int space = (last_busy - first_busy);
                if (space != busy) //window detected
                {
                    int window_size = space-2;
                    score += points_for_one_slot_window_in_day*window_size;
                    // cout << " window detected! size: " << window_size;
                }

            }
             // cout << endl;
            startDay += SLOTS_PER_DAY;
            endDay += SLOTS_PER_DAY;
        }

    }

    return score;
}

