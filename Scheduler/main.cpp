#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>

#include "solutionevaluator.h"
#include "loaders.hpp"
#include "utils.hpp"

//globals
Groups g_groups{};
Teachers g_teachers{};
Classrooms g_classrooms{};
Subjects g_subjects{};
Days g_days = createDaysVector();
Slots g_slots = createSlotsVector(g_days);

bool starts_with(string line, string prefix)
{
    return (line.substr(0, prefix.size()) == prefix);
}

void loadClasses(string file_name)
{
    ifstream ifs(file_name);
    if(!ifs.good())
    {
        cout << "Error file open" <<endl;
        ifs.close();
        exit(1);
    }
    cout << "File succesfully opened" << endl;

    for(string line; getline( ifs, line ); )
    {
        if(starts_with(line, "#"))
            continue;
        if(starts_with(line, "%GRUPA"))
            g_groups.push_back(getGroup(ifs));
        else if(starts_with(line, "%PROWADZACY"))
            g_teachers.push_back(getTeacher(ifs));
        else if(starts_with(line, "%SALA"))
            g_classrooms.push_back(getClassroom(ifs));
        else if(starts_with(line, "%PRZEDMIOT"))
            g_subjects.push_back(getSubject(ifs));
    }

}

void findMatch(Solution& solution, size_t lessonId)
{
    Lesson& lesson = solution.lessons[lessonId];
    Subject& subject = solution.subjects[lesson.subjectId];
    Group& group = solution.groups[subject.idGroup];
    Teacher& teacher = g_teachers[subject.idTeacher];
    for (int classroomId : random_order(solution.classrooms.size()))
    {
        Classroom& classroom = solution.classrooms[classroomId];
        if(contains(subject.classRoomsAvalibility, classroom.id))
            for (int slotId : random_order(g_slots.size()))
            {
                Slot& slot = g_slots[slotId];
                if(group.checkAvailability(slot.id) && classroom.checkAvailability(slot.id) && teacher.checkAvailability(slot.id))
                {
                    group.setAvailability(slot.id, false);
                    classroom.setAvailability(slot.id,false);
                    teacher.setAvailability(slot.id,false);
                    solution.lessons[lessonId].id_classroom = classroom.id;
                    solution.lessons[lessonId].id_group = group.id;
                    solution.lessons[lessonId].id_slot = slot.id;
                    solution.lessons[lessonId].id_teacher = teacher.id;
                   // solution.slots[slotId].lessons.push_back(lesson);
                    return;
                }
            }
    }
}

Solution createStartingSolution()
{
    Solution solution;

    solution.lessons = createLessonsVector(g_subjects);
    solution.groups = g_groups;
    solution.subjects = g_subjects;
    solution.classrooms = g_classrooms;

    for(size_t lessonId=0; lessonId < solution.lessons.size(); lessonId++)
        findMatch(solution, lessonId);

    return solution;
}

void displaySolution(const Solution& solution)
{
    for (auto const& group :solution.groups)
    {
        cout<< "\nGrupa " << group.name<<endl;
        for(auto const& day: g_days)
        {
            cout << "\t"<<day.name<<":" << endl;
            for(auto const& slot : day.slots)
            {
                cout<< slot.id%SLOTS_PER_DAY <<"\t- " << slot.text;
                if(group.checkAvailability(slot.id))
                {
                    cout<<"\t - X"<<endl;
                    continue;
                }
                else
                    for(auto const& lesson : solution.lessons)
                        if (lesson.id_group==group.id && lesson.id_slot == slot.id)
                            cout<<"\t- Grupa " << group.name <<
                                  ", przedmiot: " << g_subjects[lesson.subjectId].name <<
                                  ", sala: " << g_classrooms[lesson.id_classroom].address << " " << g_classrooms[lesson.id_classroom].place << endl;



            }
        }
    }
}

void tryChangeClassroom(Solution& solution, int lessonId)
{
    Lesson& lesson = solution.lessons[lessonId];
    Subject& subject = solution.subjects[ lesson.subjectId ];
    //iteruj po wszystkich salach
    for (int classroomId : random_order(solution.classrooms.size()))
    {
        //sprawdz czy pasuja do przedmiotu
        if( subject.checkClassroomApplicable(classroomId) )
        {
            Classroom& classroom = solution.classrooms[classroomId];
            //sprawdz czy sa dostepne w tym czasie
            if( classroom.checkAvailability(lesson.id_slot) )
            {
                solution.classrooms[lesson.id_classroom].setAvailability( lesson.id_slot, true );
                classroom.setAvailability( lesson.id_slot, false );
                lesson.id_classroom = classroomId;
                return;
            }
        }
    }
}


void tryChangeTimeslot(Solution& solution, int lessonId)
{
    Lesson& lesson = solution.lessons.at(lessonId);
    Group& group = solution.groups.at(lesson.id_group);
    Classroom& classroom = solution.classrooms.at(lesson.id_classroom);
    Teacher& teacher = g_teachers.at(lesson.id_teacher);
    //iteruj po wszystkich timeslotach
    for (int timeslotId : random_order(WORK_DAYS*SLOTS_PER_DAY))
    {
        //sprawdz czy sala i grupa sa dostepne w tym czasie
        if( group.checkAvailability(timeslotId) &&
            classroom.checkAvailability(timeslotId) &&
            teacher.checkAvailability(timeslotId) )
        {
            group.setAvailability( lesson.id_slot, true );
            classroom.setAvailability( lesson.id_slot, true );
            teacher.setAvailability( lesson.id_slot, true );
            group.setAvailability( timeslotId, false );
            classroom.setAvailability( timeslotId, false );
            teacher.setAvailability( timeslotId, false );
            lesson.id_slot = timeslotId;
            return;
        }

    }
}


void trySwitchLessons(Solution& solution, int firstLessonId)
{
    Lesson& firstLesson = solution.lessons[firstLessonId];
    Subject& firstSubject = solution.subjects[ firstLesson.subjectId ];
    Group& firstGroup =  solution.groups[firstLesson.id_group];
    Teacher& firstTeacher =  g_teachers[firstLesson.id_teacher];
    int firstTimeslot = firstLesson.id_slot;
    for (int secondLessonId : random_order(solution.lessons.size()))
    {
        Lesson& secondLesson = solution.lessons[secondLessonId];
        Subject& secondSubject = solution.subjects[ secondLesson.subjectId ];
        Group& secondGroup =  solution.groups[secondLesson.id_group];
        Teacher& secondTeacher =  g_teachers[secondLesson.id_teacher];
        int secondTimeslot = secondLesson.id_slot;

        //check classrooms can be switched
        if( firstSubject.checkClassroomApplicable(secondLesson.id_classroom) &&
            secondSubject.checkClassroomApplicable(firstLesson.id_classroom) )
        {
            //its the same group or two different can be switched
            if( ( firstLesson.id_group == secondLesson.id_group ) ||
                (     firstGroup.checkAvailability(secondTimeslot) &&
                      secondGroup.checkAvailability(firstTimeslot)
                )
              )
            {
                if( ( firstLesson.id_teacher == secondLesson.id_teacher ) ||
                    (     firstTeacher.checkAvailability(secondTimeslot) &&
                          secondTeacher.checkAvailability(firstTimeslot)
                    )
                  )
                {
                    int tmpClassroom, tmpTimeslot;
                    tmpClassroom = secondLesson.id_classroom;
                    tmpTimeslot = secondLesson.id_slot;
                    secondLesson.id_classroom = firstLesson.id_classroom;
                    secondLesson.id_slot = firstLesson.id_slot ;
                    firstLesson.id_classroom = tmpClassroom;
                    firstLesson.id_slot = tmpTimeslot;

                    if( firstLesson.id_group != secondLesson.id_group )
                    {
                        firstGroup.setAvailability(firstLesson.id_slot, false);
                        firstGroup.setAvailability(secondLesson.id_slot, true);
                        secondGroup.setAvailability(secondLesson.id_slot, false);
                        secondGroup.setAvailability(firstLesson.id_slot, true);
                    }

                    if( firstTeacher.id != secondTeacher.id )
                    {
                        firstTeacher.setAvailability(firstLesson.id_slot, false);
                        firstTeacher.setAvailability(secondLesson.id_slot, true);
                        secondTeacher.setAvailability(secondLesson.id_slot, false);
                        secondTeacher.setAvailability(firstLesson.id_slot, true);
                    }



                    return;
                }
            }
        }
    }
}

void changeSolution(Solution& solution)
{
    double nextChange =  random_double();
    int lessonId = random_int(solution.lessons.size());

    if(nextChange < 0.3)
    {
        tryChangeClassroom( solution, lessonId );
        return;
    }
    else if(nextChange < 0.7)
    {
        tryChangeTimeslot( solution, lessonId );
        return;
    }
    else
    {
        trySwitchLessons( solution, lessonId );
        return;
    }
}


void printLessonInfo(Solution& solution, int id )
{
    cout << "========================================================" <<endl;
    cout<<"Lesson chosen:"<<2<<endl;
    cout<<"subjectId:"<<solution.lessons[id].subjectId<<endl;
    cout<<"id_group:"<<solution.lessons[id].id_group<<endl;
    cout<<"id_teacher:"<<solution.lessons[id].id_teacher<<endl;
    cout<<"id_classroom:"<<solution.lessons[id].id_classroom<<endl;
    cout<<"id_slot:"<<solution.lessons[id].id_slot<<endl;
}

void checkModification(Solution& solution)
{
    printLessonInfo(solution, 2);
    tryChangeClassroom(solution,2);
    displaySolution(solution);

    printLessonInfo(solution, 2);
    tryChangeTimeslot(solution,2);
    displaySolution(solution);

    printLessonInfo(solution, 2);
    trySwitchLessons(solution,4);
    displaySolution(solution);
}


double acceptanceProbability(int energy, int newEnergy, double temperature)
{
    // If the new solution is better, accept it
    if (newEnergy < energy)
        return 1.0;

    // If the new solution is worse, calculate an acceptance probability
    return exp((energy - newEnergy) / temperature);
}

using time_point = std::chrono::steady_clock::time_point;


int main(/*int argc, char *argv[]*/)
{
    loadClasses("../Scheduler/input2.txt");
    Solution solution = createStartingSolution();
    displaySolution(solution);
    vector<Solution> solutions;

    double T = 10000;
    double coolingRate = 0.001;
    Solution best; //you simply the Best!

    long iterations = 0;
    SolutionEvaluator evaluateSolution(g_days, g_classrooms);
    evaluateSolution(solution);
    int startEnergy = solution.energy;
    cout << "Starting solution points: " << startEnergy <<endl;
    time_point startAnnealing = std::chrono::steady_clock::now();
    while(T > 1)
    {
        solutions.push_back(solution);

        int currentEnergy = evaluateSolution(solution);
        //cout << "currentEnergy = " << currentEnergy << endl;

        changeSolution(solution);

        int newSolutionEnergy =  evaluateSolution(solution);

        if (acceptanceProbability(currentEnergy, newSolutionEnergy, T) > random_double())
            solution = solutions.back();

        if (solution.energy > best.energy)
            best = solution;

        T *= 1.0 - coolingRate;

        iterations++;
    }
    time_point endAnnealing = std::chrono::steady_clock::now();

    cout << "========================================================" <<endl;
    displaySolution(best);
    evaluateSolution(best);
    cout << "Found best schedule with " << best.energy << " points, after " << iterations << " iterations in "
         << std::chrono::duration_cast<std::chrono::milliseconds>(endAnnealing - startAnnealing).count() <<"ms." << endl;

    return 0;
}
