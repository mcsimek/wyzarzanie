#ifndef UTILS_HPP
#define UTILS_HPP

//#include "structures.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <random>
#include <algorithm>
#include <functional>


std::random_device rd;
std::mt19937 rng(rd());

double random_double()
{
    std::uniform_int_distribution<size_t> uni(0, 100);
    return static_cast<double>(uni(rng))/100.0;
}

int random_int(int bound)
{
    std::uniform_int_distribution<> uni(0, bound-1);
    return uni(rng);
}

vector<int> random_order(int bound) {
    vector<int> ret(bound);
    std::uniform_int_distribution<size_t> uni(0, bound-1);
    generate(begin(ret), end(ret), std::bind(uni, rng));

    return ret;
}


Subject findSubject(Solution solution, Lesson lesson) {
    for (Subject subject : solution.subjects) {
        if(lesson.subjectId==subject.id)
            return subject;
    }
    return {};
}

Group findGroup(Solution solution, Subject subject) {
    for (Group group : solution.groups) {
        if(subject.idGroup==group.id)
            return group;
    }
    return {};
}

Group findGroup(Solution solution, Lesson lesson) {
    return findGroup(solution,findSubject(solution,lesson));
}

Days createDaysVector()
{
    Days ret;
    for(int i=0; i<WORK_DAYS;i++) {
        ret.push_back(Day(i));
    }
    return ret;
}

Slots createSlotsVector(Days& days)
{
    Slots ret;
    for(Day& day : days)
        for(int i=0;i<SLOTS_PER_DAY;i++)
            ret.push_back(day.slots[i]);

    return ret;
}

Lessons createLessonsVector(Subjects& subjects)
{
    Lessons ret;
    for(Subject subject : subjects)
        for(int i=0;i<subject.lessonsPerWeek;i++)
            ret.push_back(subject.lessons[i]);

    return ret;
}
//Teacher findTeacher(Solution solution, Subject subject) {
//    for (Teacher teacher : solution.teachers) {
//        if(subject.idTeacher==teacher.id)
//            return teacher;
//    }
//    return null;
//}

#endif
